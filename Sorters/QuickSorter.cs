﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interfaces;
using System.Collections;

namespace Sorters
{
    public class QuickSorter : ISorter
    {
        /// <summary>
        /// Quick sort algorithm - currently ~8000 ticks for 10000 ints (random)
        /// </summary>
        /// <typeparam name="T">Type supporting IComparable</typeparam>
        /// <param name="unsorted">array to sort</param>
        /// <returns>sorted array</returns>
        public T[] Sort<T>(T[] unsorted)
            where T : IComparable<T>, IComparable
        {
            q_sort(0, unsorted.Length - 1, unsorted);
            return unsorted;
        }

        private void q_sort<T>(int left, int right, T[] array)
            where T : IComparable<T>, IComparable
        {            
            // set hold values for recursion
            int l_hold = left;
            int r_hold = right;
            
            // TODO: find better starting pivot                        
            T pivot = array[left];
            int pivot_ix = left;                        

            // while our left index is less than right
            while (left < right)
            {
                // and while the item is larger than the pivot (stops when right indexed item is not longer larger)
                while ((array[right].CompareTo(pivot) == 1) && (left < right))
                {
                    // move right index towards pivot
                    right--;
                }

                // if they are not the same index (if they are we are at the pivot)
                if (left != right)
                {
                    // move smaller item to the left of the pivot
                    array[left] = array[right];
                    // move towards pivot
                    left++;
                }

                // while the left index item is less than pivot move indexer towards the pivot
                while ((array[left].CompareTo(pivot) == -1) && (left < right))
                {
                    // move towards pivot
                    left++;
                }

                // if they are not the same index (if they are we are at the pivot)
                if (left != right)
                {
                    // move larger item to the right of the pivot
                    array[right] = array[left];
                    // move towards pivot
                    right--;
                }
            }

            //items are now arranged around the pivot

            // set new pivot
            array[left] = pivot;
            pivot_ix = left;

            // set left and right for recursive calls to the hold values declared above (first run is 0 and length - 1)
            left = l_hold;            
            right = r_hold;

            //Potential Issue: c# doesn't use tail call optimization, so call stack may ballon memory usage [O(log(n)) stack space]
            if (left < pivot_ix)
            {
                q_sort(left, pivot_ix - 1, array);
            }

            if (right > pivot_ix)
            {
                q_sort(pivot_ix + 1, right, array);
            }
        }
    }
}
