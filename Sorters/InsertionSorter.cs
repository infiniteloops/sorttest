﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interfaces;

namespace Sorters
{
    public class InsertionSorter:ISorter
    {
        public T[] Sort<T>(T[] unsorted) where T : IComparable, IComparable<T>
        {
            int i;
            int j;
            T index;

            for (i = 1; i < unsorted.Length; i++)
            {
                index = unsorted[i];
                j = i;

                while ((j > 0) && (unsorted[j - 1].CompareTo(index) == 1))
                {
                    unsorted[j] = unsorted[j - 1];
                    j--;
                }

                unsorted[j] = index;
            }

            return unsorted;
        }        
    }
}
