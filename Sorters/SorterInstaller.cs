﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.MicroKernel.Registration;
using Interfaces;

namespace Sorters
{
    public class SorterInstaller : IWindsorInstaller
    {
        public void Install(Castle.Windsor.IWindsorContainer container, Castle.MicroKernel.SubSystems.Configuration.IConfigurationStore store)
        {
            container.Register(Component.For<ISorter>().ImplementedBy<QuickSorter>().LifestyleTransient().Named("quick"));
            container.Register(Component.For<ISorter>().ImplementedBy<BubbleSorter>().LifestyleTransient().Named("bubble"));
            container.Register(Component.For<ISorter>().ImplementedBy<InsertionSorter>().LifestyleTransient().Named("insert"));
        }
    }
}
