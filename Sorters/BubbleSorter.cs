﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Interfaces;

namespace Sorters
{
    public class BubbleSorter : ISorter
    {
        public T[] Sort<T>(T[] unsorted) where T : IComparable, IComparable<T>
        {
            T[] sorted = new T[unsorted.Length];
            int inplaceIndex = unsorted.Length - 1;
            bool swap = true;
            // continue as long as a swap happens
            while (swap)
            {
                swap = false;
                for (int i = 0; i <= inplaceIndex; i++)
                {                    
                    if (i == inplaceIndex)
                    {
                        inplaceIndex--;
                        continue;
                    }
                    T maxT = unsorted[i];

                    if (maxT.CompareTo(unsorted[i + 1]) == 1)
                    {
                        unsorted[i] = unsorted[i + 1];
                        unsorted[i + 1] = maxT;
                        swap = true;
                    }
                    else if (maxT.CompareTo(unsorted[i + 1]) == -1)
                    {
                        maxT = unsorted[i + 1];
                    }
                }
            }
            return unsorted;
        }
    }
}
