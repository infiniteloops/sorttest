﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interfaces
{
    public interface ISorter
    {
        T[] Sort<T>(T[] unsorted) where T : IComparable, IComparable<T>;
    }
}
