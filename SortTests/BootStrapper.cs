﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Windsor;
using Castle.MicroKernel.Registration;

namespace SortTests
{
    public static class BootStrapper
    {        
        public static IWindsorContainer Init()
        {
            IWindsorContainer container = new WindsorContainer();
            AddInstallers(container);
            return container;
        }

        private static void AddInstallers(IWindsorContainer container)
        {
            container.Register(Types.FromThisAssembly().BasedOn<IWindsorInstaller>().WithServiceBase());
            container.Register(Types.FromAssemblyNamed("Sorters").BasedOn<IWindsorInstaller>().WithServiceBase());
            var installers = container.ResolveAll<IWindsorInstaller>();
            container.Install(installers);
        }
    }
}
