﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Windsor;
using Interfaces;
using System.Diagnostics;

namespace SortTests
{
    class Program
    {
        static IWindsorContainer Container { get; set; }

        static Program()
        {
            Container = BootStrapper.Init();
        }

        static void Main(string[] args)
        {
            Stopwatch timer = new Stopwatch();
            ISorter bubbleSorter = Container.Resolve<ISorter>("bubble");
            ISorter insertSorter = Container.Resolve<ISorter>("insert");
            ISorter quickSorter = Container.Resolve<ISorter>("quick");

            int arrayLength = 10000;

            int[] array = GenerateArray(arrayLength);
            var bubbleArray = new int[arrayLength];
            var insertArray = new int[arrayLength];
            var quickArray = new int[arrayLength];
            var linqArray = new int[arrayLength];
            array.CopyTo(bubbleArray, 0);
            array.CopyTo(insertArray, 0);
            array.CopyTo(quickArray, 0);
            array.CopyTo(linqArray, 0);

            timer.Start();
            int[] sorted = bubbleSorter.Sort(bubbleArray);
            timer.Stop();
            Console.WriteLine("Bubble sort took {0} ticks", timer.ElapsedTicks);
            
            timer.Reset();
            timer.Start();
            int[] sorted2 = insertSorter.Sort(insertArray);
            timer.Stop();
            Console.WriteLine("Insertion sort took {0} ticks", timer.ElapsedTicks);

            timer.Reset();
            timer.Start();
            int[] sorted3 = quickSorter.Sort(quickArray);
            timer.Stop();
            Console.WriteLine("Quick sort took {0} ticks", timer.ElapsedTicks);

            timer.Reset();
            timer.Start();
            int[] sorted4 = linqArray.OrderByDescending(t => t).ToArray();
            timer.Stop();
            Console.WriteLine("Linq sort (order by) took {0} ticks", timer.ElapsedTicks);

            timer.Reset();
            timer.Start();
            Array.Sort(array);
            timer.Stop();
            Console.WriteLine("Array.Sort sort took {0} ticks", timer.ElapsedTicks);

            Console.ReadKey(true);
        }

        private static int[] GenerateArray(int length)
        {
            int[] array = new int[length];
            Random rnd = new Random();
            for (int i = 0; i < length; i++)
            {
                int n = rnd.Next(1, length);
                array[i] = n;
            }
            return array;
        }
    }
}
